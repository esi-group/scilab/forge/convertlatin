// Allan CORNET - DIGITEO - 2011

// convertlatin.tst saved as a utf-8 file
ref_UTF8 = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";
if size(ascii(ref_UTF8), "*") <> 128 then pause, end

length_UTF8 = length(ref_UTF8);
if length_UTF8 <> 64 then pause, end

latin = utf8tolatin(ref_UTF8);
length_latin = length(latin);
if length_latin <> 64 then pause, end

ascii_latin = ascii(latin);
for i = 1:length_latin
  if 191 + i <> ascii_latin(i) then pause, end
end

converted_UTF8 = latintoutf8(latin);
if size(ascii(converted_UTF8), "*") <> 128 then pause, end

if or(ascii(converted_UTF8) <> ascii(ref_UTF8)) then pause, end

texts = ["中文測試", ..
         "азеазеясдвклгхклмвцмзер" ..
         "人 人 生 而 自 由, 在 尊 严 和 权 利 上 一 律 平 等。 他 们 赋 有 理 性 和 良 心, 并 应 以 兄 弟 关 系 的 精 神 相 对 待。"]; 

if or(ascii(latintoutf8(texts)) <> ascii(texts)) then pause, end
