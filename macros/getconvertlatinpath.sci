// ====================================================================
// Allan CORNET
// DIGITEO 2011
// ====================================================================
//
function s = getconvertlatinpath()
  [fs, thislibpath] = libraryinfo("convertlatinlib");
  s = pathconvert(fullpath(thislibpath + "../"), %t, %t);
endfunction
// ====================================================================
