//
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//

function demo_utftolatin()

  disp("");
  disp("demo utf-8 to latin encoding:");
  
  latin = ascii(233);
  disp("ascii(233) is """ + ascii(233) + """ as latin encoding.");
  
  utf8 = ascii([195 169]);
  disp("ascii([195 169]) is """ + utf8 + """ as utf-8 encoding.");
  
  str =  msprintf("%s%s%s", "ascii(utf8tolatin(ascii([195 169]))) returns ", ..
                            string(ascii(utf8tolatin(utf8))), ..
                            " as latin encoding.");
  disp(str);
  
  editor(fullpath(get_absolute_file_path("utf8tolatin.dem.sce") + "/utf8tolatin.dem.sce"), "readonly");
  
endfunction

demo_utftolatin();
clear demo_utftolatin;

